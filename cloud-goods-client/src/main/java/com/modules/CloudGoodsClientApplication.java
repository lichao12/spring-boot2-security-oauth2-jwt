package com.modules;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudGoodsClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudGoodsClientApplication.class, args);
    }

}
