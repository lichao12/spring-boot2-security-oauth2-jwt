# spring-boot2-security-oauth2-jwt

#### 介绍
基于Spring Boot2 、MyBatis、 Spring Security、MySql、OAuth2构建一个易理解、高可用、高扩展性的分布式单点登录WEB应用基层系统

#### 技术方案

核心框架：Spring Boot2

ORM框架：Mybatis

安全框架：Spring Security、OAuth2

数据库：MySqL

#### 使用说明

1. 使用IDEA导入本项目
2. 新建数据库;
3. 导入数据库sql/sql.sql
4. 修改(resources/application.yml)配置文件，主要修改数据配置
5. 运行项目
    5.1. 项目根目录下执行mvn springboot:run
    5.2. 直接运行Application.java
6. 服务器端浏览器访问：http://localhost:8087/sso
7. 客户端1浏览器访问：http://localhost:8086
8. 客户端2浏览器访问：http://localhost:8085

#### 用户密码
账号：admin 密码：123
![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/171603_959ba58a_1108042.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0515/171548_99074963_1108042.png "屏幕截图.png")
